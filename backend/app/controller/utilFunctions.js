const smtpTransport = require("./mailer"),
  config = require("../../settings"),
  fs = require('fs');
  path = require('path');

/**
 * sendMail
 * @param {email,contactEmails,fullName,message} data
 * @param {message} callback
 */
exports.sendMail = (data, callback) => {
  try {

    let html = fs.readFileSync(path.resolve(".") + '/templates/template.html', 'utf8');
    html = html.replace("__NAME__", data.fullName);
    html = html.replace("__EMAIL__", data.email);
    html = html.replace("__EMAIL__", data.email);
    html = html.replace("__MESSAGE__", data.message);
    html = html.replace("__REFERENCE__", data.refferalURL);

    var mailOptions = {
      from: data.email,
      to: '" Talkroom "' + config.contactEmails,
      subject: data.fullName + " have contacted",
      html: html
    };
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, (err, response) => {
      if (err) {
        return callback(err, null);
      } else {
        return callback(null, {
          message: "Email send successfully"
        });
      }
    });
  } catch (err) {
    return callback(err, null);
  }
}; //sendMail

/**
 * formatedResponse
 * @param {reqStatus} reqStatus ,type:boolean
 * @param {response} callback
 */
exports.formatedResponse = (reqStatus, result, msg) => {
  let response = {};
  response.status = true;
  response.code = 200;
  response.msg = msg;
  response.result = result;

  if (!reqStatus) {
    response.status = false;
    response.code = 500;
    response.result = [];
  }
  return response;
}; //formatedResponse
