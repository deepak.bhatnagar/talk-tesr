import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactUsRouting } from './contact-us.routing';
import { ContactUsComponent } from './contact-us.component';

@NgModule({
    imports: [
        ContactUsRouting,
        CommonModule,
    ],
    declarations: [ ContactUsComponent ],
    providers: []
})

export class ContactUsModule { }



