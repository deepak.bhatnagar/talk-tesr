// import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeRouting } from './home.routing';

import { HomeComponent } from './home.component';
import { HomeFormComponent } from '../_homeForm/home-form.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [
    HomeComponent,
    HomeFormComponent
  ],
  imports: [
    SharedModule,
    HomeRouting,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModule
  ],
  providers: [],
})
export class HomeModule { }
