import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Validators } from '../shared/validator';
import { UrlService } from '../shared/app.url.service';
import { AuthSessionService } from '../_guards/auth.session.service';

declare var $: any;

@Component({
    selector: 'home-form',
    templateUrl: './home-form.component.html'
})
export class HomeFormComponent implements OnInit {

    constructor(
                    public urlService: UrlService,
                    private formBuilder: FormBuilder,
                    private router: Router,
                ) { }

    loginForm: FormGroup;
    errorMsg = '';
    ngOnInit() {
        $('body').addClass('before-login');
        this.loginForm = this.formBuilder.group({
            roomname: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.validateAlphabetWtihoutSpace])],
        });

        $('.form-control').on('focus blur', function(e) {
            console.log(e.type, this.value.length);
            $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
    }
    onSubmit(model) {
        console.log(model);
        this.router.navigate(['call/' + model.roomname]);
    }
}
