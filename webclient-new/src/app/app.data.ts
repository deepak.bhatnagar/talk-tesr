export class My {
    public static user: any;
    public static isVideoCall = false
    public static queryParams = ''
    public static getProfilePath(): string {
        if (My.user) {
            return My.user.profilePath;
        } else {
            return '';
        }
    }
    public static getUserId(): string {
        if (My.user) {
            return My.user.userId;
        } else {
            return '';
        }
    }
    public static getDisplayName(): string {
        if (My.user) {
            return My.user.displayName;
        } else {
            return '';
        }
    }
    public static setSessionUser(user: any) {
        My.user = user;
    }
    public static getSessionUser(): any {
        if(My.user)
            return My.user;
        else
            return null;
    }
    public static isGuest() {
        return My.user.isGuest;
    }
    public static setVideoCall(status) {
        My.isVideoCall = status;
    }
    public static getVideoCall() {
        return My.isVideoCall;
    }
    public static setQueryParams(queryParams) {
        My.queryParams = queryParams        
    }
    public static getQueryParams() {
        return My.queryParams;
    }
}

// TypeScript
interface IUser {
    userId: string;
    displayName: string;
    firstName: string;
    lastName: string;
    email: string;
    online: boolean;
    unread: number;
    username: string;
    profilePath: string;
}

export class Users {
    public static lists: { [userId: string]: IUser } = {};

    public static get(userId?: string): any {
        let arr: Array<IUser> = [];
        if ( userId && !Users.lists[userId] ) {
            return {};
        }
        if ( userId ) {
            return Users.lists[userId];
        }
        for ( let userId in  Users.lists ) {
            if ( Users.lists.hasOwnProperty( userId ) && Users.lists[userId].userId ) {
                arr.push( Users.lists[userId] );
            }
        }
        return arr;
    }
    public static getUsers(userIds: Array<string>): Array<IUser> {
        let arr: Array<IUser> = [];
        if (userIds.length) {
            for ( let userId in  Users.lists ) {
                if ( Users.lists.hasOwnProperty( userId ) && Users.lists[userId].userId ) {
                    let index = userIds.indexOf(userId);
                    if (index > -1) {
                        arr.push( Users.lists[userId] );
                    }
                }
            }
        }
        return arr;
    }
    public static insert(arr: Array<IUser>) {
        for (let index = 0; index < arr.length; index++) {
            Users.lists[arr[index].userId] = arr[index];
        }
    }
    public static addEdit(user: IUser) {
        if (Users.lists[user.userId]) {
            console.log( 'user edit' );
        } else {
            console.log( 'user add' );
        }
        if (user.userId !== My.user.userId) {
            Users.lists[user.userId] = user;
        } else {
            console.error('cant add myself to userlist');
        }
    }
    public static updateValues( userId: string,  obj: Object) {
        for ( let key in obj ) {
            if ( obj.hasOwnProperty( key ) && Users.lists[userId][key] ) {
                Users.lists[userId][key] = obj[key];
            }
        }
    }
    public static delete(userId: string) {
        if ( Users.lists[userId] ) {
            delete Users.lists[userId];
        } else {
            console.warn('offline undefined', userId);
        }
    }
    public static online(userId: string) {
        console.log('online', Users.lists);
        if ( Users.lists[userId] ) {
            Users.lists[userId].online = true;
        } else {
            console.warn('offline undefined', userId);
        }
    }
    public static offline(userId: string) {
        if ( Users.lists[userId] ) {
            Users.lists[userId].online = false;
        } else {
            console.warn('offline undefined', userId);
        }
    }
    public static unreadPlus(userId: string) {
        if ( Users.lists[userId] ) {
            Users.lists[userId].unread = Users.lists[userId].unread + 1;
        } else {
            console.warn('offline undefined', userId);
        }
    }
    public static unreadZero(userId: string) {
        if ( Users.lists[userId] ) {
            Users.lists[userId].unread = 0;
        } else {
            console.warn('offline undefined', userId);
        }
    }
    public static getUnread(userId: string) {
        if ( Users.lists[userId] ) {
            return Users.lists[userId].unread;
        } else {
            console.warn('offline undefined', userId);
        }
        return 0;
    }
    public static getOnOffUsers( is: boolean ) {
        let arr: Array<number> = [];
        for ( let userId in  Users.lists ) {
            if ( Users.lists.hasOwnProperty( userId ) &&  Users.lists[userId].userId ) {
                if ( is === Users.lists[userId].online ) {
                    arr.push( parseInt(userId) );
                }
            }
        }
        return arr;
    }
}

// TypeScript
interface IGroup {
    groupId: string;
    // ownerId: string;
    profilePath: string;
    groupName: string;
    grouptype: string;
    description: string;
    members: Array<string>;
    admins: Array<string>;
    unread: number;
}

export class Groups {
    public static lists: { [groupId: string]: IGroup } = {};

    public static get(groupId?: string): any {
        let arr: Array<IGroup> = [];
        if ( groupId && !Groups.lists[groupId] ) {
            return {};
        }
        if ( groupId ) {
            return Groups.lists[groupId];
        }
        for ( let groupId in  Groups.lists ) {
            if ( Groups.lists.hasOwnProperty( groupId ) && Groups.lists[groupId] ) {
                arr.push( Groups.lists[groupId] );
            }
        }
        return arr;
    }

    public static getGroupIds() {
        let arr: Array<string> = [];
        for ( let groupId in  Groups.lists ) {
            if ( Groups.lists.hasOwnProperty( groupId ) && Groups.lists[groupId].groupId ) {
                arr.push( Groups.lists[groupId].groupId );
            }
        }
        return arr;
    }

    public static insert( arr: Array<IGroup> ) {
        for (let index = 0; index < arr.length; index++) {
            Groups.lists[arr[index].groupId] = arr[index];
        }
        console.log('insert group', Groups.lists);
    }

    public static addEdit(group: IGroup) {
        if (Groups.lists[group.groupId]) {
            console.log( 'group edit' );
        } else {
            console.log( 'group add' );
        }
        Groups.lists[group.groupId] = group;
    }
    public static updateValues( groupId: string,  obj: Object) {
        for ( let key in obj ) {
            if ( obj.hasOwnProperty( key ) && Groups.lists[groupId][key] ) {
                Groups.lists[groupId][key] = obj[key];
            }
        }
    }
    public static delete(groupId: string) {
        if ( Groups.lists[groupId] ) {
            delete Groups.lists[groupId];
        } else {
            console.warn( 'groupid undefined', groupId );
        }
    }

    public static unreadPlus(groupId: string) {
        if ( Groups.lists[groupId] ) {
            Groups.lists[groupId].unread = Groups.lists[groupId].unread + 1;
        } else {
            console.warn( 'groupid undefined', groupId );
        }
    }
    public static unreadZero(groupId: string) {
        if ( Groups.lists[groupId] ) {
            Groups.lists[groupId].unread = 0;
        } else {
            console.warn( 'groupid undefined', groupId );
        }
    }
    public static getUnread(groupId: string) {
        if ( Groups.lists[groupId] ) {
            return Groups.lists[groupId].unread;
        } else {
            console.warn( 'groupid undefined', groupId );
        }
        return 0;
    }
}
