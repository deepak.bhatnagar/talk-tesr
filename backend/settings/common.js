let config = {};

var moonLanding = new Date();
config.COPYRIGHTYEAR = moonLanding.getFullYear();
config.allowedExt = [
  ".js",
  ".ico",
  ".css",
  ".png",
  ".jpg",
  ".jpeg",
  ".gif",
  ".woff2",
  ".woff",
  ".ttf",
  ".svg",
  ".PNG",
  ".JPG",
  ".GIF",
  ".WOFF2",
  ".WOFF",
  ".TTF",
  ".SVG",
  ".JPEG",
  ".webm",
  ".mp3",
  ".otf"
];
config.msgs = {
  signupSuccess: "You have successfully signed up.",
  updateSuccess: "Updated Successfully",
  emailExist: "Email already Exists",
  loginSuccess: "logged in successfully",
  invalidPassword: "Wrong password",
  invalidEmail: "Invalid email address/password or check your market.",
  invalidAccess: "Invalid Access",
  invalidToken: "Invalid Token",
  logoutSuccess: "logout Successfully",
  norecords: "no records found",
  groupCreateSuccess: "Group create successfully",
  groupExist: "Group name already exists",
  groupNotExist: "Group name not exists",
  groupUpdateSuccess: "Group update successfully",
  groupUserAddSuccess: "Group user add successfully",
  groupUserExist: "This user already exists in this group",
  emailNotExist: "Email not found.",
  emailSent: "Email sent successfully.",
  passwordUpdate: "Password update successfully.",
  emailNotVerified:
    "Please verify your account, as you have not verify your email.",
  accountVerified:
    "Thanks for email verification, your account is Active now and ready to use.",
  emailAlreadyVerified: "Your account is already verified.",
  incorrectOldPassword: "Your old password is not correct.",
  userNotActive: "You are not active user.",
  invalidURL: "The activation link is expired or have already been used."
};

module.exports = config;
