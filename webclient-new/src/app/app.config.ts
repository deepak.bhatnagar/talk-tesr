import { Location } from "@angular/common";
import { environment } from 'src/environments/environment';

export class Config {
  // ================================ HTTPS ===============================//

  // public static apiEndpoint =  'https://' + location.hostname + ':61289/';
  // public static chatEndpoint = 'https://' + location.hostname + ':61289';
  // public static callEndpoint = 'https://' + location.hostname + ':61289/call';


  public static buildVersion = "1.3.2";

  public static apiEndpoint =  `https://${location.hostname}${environment.production ? '' : ':61289'}/`;
  public static chatEndpoint = `https://${location.hostname}${environment.production ? '' : ':61289'}`;
  public static callEndpoint = `https://${location.hostname}${environment.production ? '' : ':61289'}/call`;

  // ----------------------------------------------------------------------//

  // ================================ HTTP ================================//

  // public static apiEndpoint =  'http://' + location.hostname + ':61289/';
  // public static chatEndpoint = 'http://' + location.hostname + ':61289';
  // public static callEndpoint = 'http://' + location.hostname + ':61289/call';

  // public static apiEndpoint =  'http://' + location.hostname + '/';
  // public static chatEndpoint = 'http://' + location.hostname;
  // public static callEndpoint = 'http://' + location.hostname + '/call';

  // ----------------------------------------------------------------------//
  location: Location;
  constructor(location: Location) {
    this.location = location;
  }
}
