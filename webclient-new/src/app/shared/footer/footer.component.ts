import { Component, OnInit } from '@angular/core';
import { Config } from 'src/app/app.config';

declare var $: any;


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  copyrightYear:number;
  urlHash:string = '';
  goToTop:boolean = false;
  buildVersion = Config.buildVersion;

  constructor() {
    this.urlHash = window.location.hash;
   }

  ngOnInit() {
    let self = this;
    this.copyrightYear = new Date().getFullYear();

    $(window).scroll(function () {          
      if ( getCurrentScroll() >= 150) {        
        if(self.urlHash == "#/" || self.urlHash == "#/home"){
          self.goToTop = true;
        }
      }else{
        //console.log("getCurrentScroll",getCurrentScroll());
        self.goToTop = false;
      } 
    });

    function getCurrentScroll() {
      return window.pageYOffset || document.documentElement.scrollTop;
    }
  }

  goTop(){
    window.scroll({
      top: 0,//2100, 
      left: 0, 
      behavior: 'smooth' 
    });
  }//goTop


}
