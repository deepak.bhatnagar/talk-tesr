import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { PrivacyComponent } from './privacy.component';

const appRoutes: Routes = [
   { path: '', component: PrivacyComponent}
];

export const PrivacyRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );
