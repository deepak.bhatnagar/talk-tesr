import{ ModuleWithProviders } from '@angular/core';
import{ RouterModule, Routes} from '@angular/router';

import{ CallComponent } from './call.component';
import { AuthGuard } from '../_guards/auth.gaurd';
const appRoutes: Routes = [
   { path: '', component: CallComponent }
];

export const CallRouting: ModuleWithProviders = RouterModule.forChild( appRoutes );