import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
   { path: '', loadChildren: './home/home.module#HomeModule'},
   { path: 'call/:roomId', loadChildren: './call/call.module#CallModule'},
   { path: 'call/:roomId/:queryParams', loadChildren: './call/call.module#CallModule'},
   { path: 'whiteboard', loadChildren: './whiteboard/whiteboard.module#WhiteboardModule'},
   { path: 'about-us', loadChildren: './about-us/about-us.module#AboutUsModule'},
   { path: 'contact-us', loadChildren: './contact-us/contact-us.module#ContactUsModule'},
   { path: 'privacy', loadChildren: './privacy/privacy.module#PrivacyModule'},
   { path: 'page-not-found', loadChildren: './page-not-found/page-not-found.module#PageNotFoundModule' },
   { path: '**', redirectTo: '/page-not-found'},
];
export const routing: ModuleWithProviders = RouterModule.forRoot( appRoutes );
