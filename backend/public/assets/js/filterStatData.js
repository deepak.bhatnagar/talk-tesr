var StatsObj = null;
function filterStatsObj() {
    this.bytesToSize = function (bytes) {
        var k = 1000;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes <= 0) {
            return '0 Bytes';
        }
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)), 10);

        if (!sizes[i]) {
            return '0 Bytes';
        }
        var size = (bytes / Math.pow(k, i));
        return size.toPrecision(3) + ' ' + sizes[i];
    };
    function splitLines(sdp) {
        var info = {};
        sdp.split('\n').forEach(function (line) {
            if (line.indexOf('m=video') === 0) {
                info.videoCodecNumbers = [];
                line.split('SAVPF')[1].split(' ').forEach(function (codecNumber) {
                    codecNumber = codecNumber.trim();
                    if (!codecNumber || !codecNumber.length) {
                        return;
                    }
                    info.videoCodecNumbers.push(codecNumber);
                    info.videoCodecNumbersOriginal = line;
                });
            }
            if (line.indexOf('VP8/90000') !== -1 && !info.vp8LineNumber) {
                info.vp8LineNumber = line.replace('a=rtpmap:', '').split(' ')[0];
            }
            if (line.indexOf('VP9/90000') !== -1 && !info.vp9LineNumber) {
                info.vp9LineNumber = line.replace('a=rtpmap:', '').split(' ')[0];
            }
            if (line.indexOf('H264/90000') !== -1 && !info.h264LineNumber) {
                info.h264LineNumber = line.replace('a=rtpmap:', '').split(' ')[0];
            }
        });
        return info;
    }
    function preferCodec(sdp, codec, info) {
        var preferCodecNumber = '';
        if (codec === 'vp8') {
            if (!info.vp8LineNumber) {
                return sdp;
            }
            preferCodecNumber = info.vp8LineNumber;
        }

        if (codec === 'vp9') {
            if (!info.vp9LineNumber) {
                return sdp;
            }
            preferCodecNumber = info.vp9LineNumber;
        }

        if (codec === 'h264') {
            if (!info.h264LineNumber) {
                return sdp;
            }
            preferCodecNumber = info.h264LineNumber;
        }
        var newLine = info.videoCodecNumbersOriginal.split('SAVPF')[0] + 'SAVPF ';
        var newOrder = [preferCodecNumber];
        info.videoCodecNumbers.forEach(function (codecNumber) {
            if (codecNumber === preferCodecNumber)
                {
                    return;
                }
            newOrder.push(codecNumber);
        });
        newLine += newOrder.join(' ');
        sdp = sdp.replace(info.videoCodecNumbersOriginal, newLine);
        return sdp;
    };
    this.preferSelectedCodec = function (sdp) {
        var info = splitLines(sdp);
        if (codec.value === 'vp8' && info.vp8LineNumber === info.videoCodecNumbers[0]) {
            return sdp;
        }
        if (codec.value === 'vp9' && info.vp9LineNumber === info.videoCodecNumbers[0]) {
            return sdp;
        }
        if (codec.value === 'h264' && info.h264LineNumber === info.videoCodecNumbers[0]) {
            return sdp;
        }
        sdp = preferCodec(sdp, codec.value, info);
        return sdp;
    };
}

function openFilterStatsObj() {
    StatsObj = new filterStatsObj();
    return StatsObj;
}