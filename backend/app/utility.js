
'use strict';

class Utility {

    isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
        return JSON.stringify(obj) === JSON.stringify({});
    }

    deepClone(a) {
        var b = JSON.parse( JSON.stringify(a) );
        return b;
    }

    twoDigits (d) {
        if(0 <= d && d < 10) return "0" + d.toString();
        if(-10 < d && d < 0) return "-0" + (-1*d).toString();
        return d.toString();
    }

    getMysqlDate (date) {
        let myDate = date || new Date();

        let d = myDate.getUTCFullYear() + "-" + this.twoDigits(1 + myDate.getUTCMonth()) + "-" + this.twoDigits(myDate.getUTCDate()) + " " + this.twoDigits(myDate.getUTCHours()) + ":" + this.twoDigits(myDate.getUTCMinutes()) + ":" + this.twoDigits(myDate.getUTCSeconds());
        return d;
    }
    //eCode 0 = success, 1 = custom message , 2 = mysql error, 3 = server Error
    response (err,data) {
        let res = {};
        res.resData = {};
        if(err) {
            
            if(err.message) {
                res.eCode = 1;
            }else if(err.sqlMessage) {
                res.eCode = 2;
            }else{
                res.eCode = 3;
            }
        }else {
            res.eCode = 1;
        }
        if(data) {
            res.eCode = 0;
            res.resData = data;
        }   
        if(res.eCode) {
            if(res.eCode === 1 ) {
                res.errorMessage = err.message
            }else if(res.eCode === 2) {
                res.errorMessage = "Mysql Errors.";
            }else if(res.eCode === 3) {
                res.errorMessage = "Server Errors.";
            }
        }
        return res;
    }

    responseSocket (err , data) {
        let res = {};
        res.eCode = 0;
        if(err) {
            res.eCode = 1;
            res.err = err;
        }
        if(data) {
            res.data = data;
        }
        return res;
    }
};

module.exports = new Utility();
