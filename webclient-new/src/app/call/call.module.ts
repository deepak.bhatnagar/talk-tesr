//import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CallRouting } from './call.routing';


import { CallComponent } from './call.component';

import { CallService } from '../shared/call.service';
import { SharedModule } from '../shared/shared.module';

// import { WhiteboardComponent } from '../whiteboard/whiteboard.component';


@NgModule({
  declarations: [
    CallComponent,
    // WhiteboardComponent,
  ],
  imports: [
    //BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CallRouting,
    CommonModule,
    SharedModule
  ],
  providers: [ CallService],
})
export class CallModule { }