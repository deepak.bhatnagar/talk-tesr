export class emojis{
    public static list = [
        {
            emoji : '<i class="em em---1"></i>',
            class:'em em---1'
        },
        {
            emoji : '<i class="em em--1"></i>',
            class:'em em--1'
        },
        {
            emoji : '<i class="em em-astonished"></i>',
            class : 'em em-astonished'
        },
        {
            emoji : '<i class="em em-confused"></i>',
            class : 'em em-confused'
        },
        {
            emoji : '<i class="em em-cry"></i>',
            class : 'em em-cry'
        },
        {
            emoji : '<i class="em em-disappointed"></i>',
            class : 'em em-disappointed'
        },
        {
            emoji : '<i class="em em-sweet_potato"></i>',
            class : 'em em-sweet_potato'
        },
        {
            emoji : '<i class="em em-tropical_fish"></i>',
            class : 'em em-tropical_fish'
        },
        {
            emoji : '<i class="em em-u7533"></i>',
            class : 'em em-u7533'
        },
        {
            emoji : '<i class="em em-third_place_medal"></i>',
            class : 'em em-third_place_medal'
        },
        {
            emoji : '<i class="em em-stuck_out_tongue"></i>',
            class : 'em em-stuck_out_tongue'
        },
        {
            emoji : '<i class="em em-smiley"></i>',
            class : 'em em-smiley'
        },
        {
            emoji : '<i class="em em-camera"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-busts_in_silhouette"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-closed_umbrella"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-custard"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-dizzy_face"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-fearful"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-flushed"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-frowning"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-grimacing"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-hammer"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-gift_heart"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-handshake"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-hugging_face"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-hearts"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-high_heel"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-innocent"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-key"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-man-girl-girl"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-lying_face"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-male-mechanic"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-man-walking"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-man_dancing"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-man-running"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-musical_keyboard"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-negative_squared_cross_mark"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-o"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-relaxed"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-rocket"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-shamrock"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-smirk"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-stuck_out_tongue_winking_eye"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-sweat_smile"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-thinking_face"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-trophy"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-white_frowning_face"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-woman-lifting-weights"></i>',
            class : ''
        },
        {
            emoji : '<i class="em em-zap"></i>',
            class : ''
        }
        
    ];
}