import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacyRouting } from './privacy.routing';
import { PrivacyComponent } from './privacy.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PrivacyComponent],
  imports: [
    CommonModule,
    PrivacyRouting,
    SharedModule
  ]
})
export class PrivacyModule { }
